﻿using Imdb.Core.Domain;
using Imdb.Core.Repositories;
using Imdb.Infra.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Infra.Repositories
{
    public class Repository : IRepository
    {
        private readonly ImdbDbContext _dbContext;

        public Repository(ImdbDbContext dbContext) => _dbContext = dbContext;

        public async Task AddAsync<T>(T entity, CancellationToken cancellationToken) where T : Entity => await _dbContext
            .AddAsync(entity, cancellationToken);

        public void Delete<T>(T entity) where T : Entity => _dbContext
            .Set<T>()
            .Remove(entity);

        public Task<T> GetFirstOrDefaultAsync<T>(Expression<Func<T, bool>> expression, CancellationToken cancellationToken) where T : Entity => _dbContext
            .Set<T>()
            .Where(e => e.IsActive)
            .FirstOrDefaultAsync(expression, cancellationToken);

        public Task<T> GetByIdAsync<T>(Guid id, CancellationToken cancellationToken) where T : Entity => _dbContext
            .Set<T>()
            .FirstOrDefaultAsync(e => e.Id == id && e.IsActive, cancellationToken);

        public void Update<T>(T entity) where T : Entity => _dbContext
            .Set<T>()
            .Update(entity);

        public Task<bool> AnyAsync<T>(Expression<Func<T, bool>> expression, CancellationToken cancellationToken) where T : Entity => _dbContext
            .Set<T>()
            .AnyAsync(expression, cancellationToken);
    }
}
