﻿using Imdb.Infra.Mappings;
using Microsoft.EntityFrameworkCore;

namespace Imdb.Infra.Contexts
{
    public class ImdbDbContext : DbContext
    {
        public ImdbDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ActorMap());
            modelBuilder.ApplyConfiguration(new DirectorMap());
            modelBuilder.ApplyConfiguration(new GenderMap());
            modelBuilder.ApplyConfiguration(new MovieMap());
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new VoteMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
