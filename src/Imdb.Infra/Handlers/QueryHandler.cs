﻿using Imdb.Infra.Contexts;
using Libs.Cqrs.Contracts;
using System.Linq;

namespace Imdb.Infra.Handlers
{
    public class QueryHandler : IQueryHandler
    {
        private readonly ImdbDbContext _dbContext;

        public QueryHandler(ImdbDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<R> Handle<T, R>(IQuery<T, R> query)
            where T : class
            where R : class => query.Query(_dbContext.Set<T>());

        public IQueryable<T> Handle<T>(IQuery<T> query) where T : class => query.Query(_dbContext.Set<T>());
    }
}
