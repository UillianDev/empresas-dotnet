﻿using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Imdb.Infra.Mappings
{
    public class ActorMap : MovieComponentMap<Actor>
    {
        public override void Configure(EntityTypeBuilder<Actor> builder)
        {
            base.Configure(builder);

            builder.HasMany(actor => actor.Movies)
                .WithMany(movie => movie.Actors);
        }
    }
}
