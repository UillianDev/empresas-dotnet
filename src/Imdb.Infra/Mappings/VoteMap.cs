﻿using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Imdb.Infra.Mappings
{
    public class VoteMap : EntityMap<Vote>
    {
        public override void Configure(EntityTypeBuilder<Vote> builder)
        {
            base.Configure(builder);

            builder.Property(e => e.Score);

            builder.HasOne(e => e.Movie)
                .WithMany(movie => movie.Votes)
                .IsRequired();

            builder.HasOne(e => e.User)
                .WithMany(user => user.Votes);
        }
    }
}
