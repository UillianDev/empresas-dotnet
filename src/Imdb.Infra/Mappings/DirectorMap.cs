﻿using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Imdb.Infra.Mappings
{
    public class DirectorMap : MovieComponentMap<Director>
    {
        public override void Configure(EntityTypeBuilder<Director> builder)
        {
            base.Configure(builder);

            builder.HasMany(director => director.Movies)
                .WithMany(movie => movie.Directors);
        }
    }
}
