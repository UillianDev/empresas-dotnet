﻿using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Imdb.Infra.Mappings
{
    public class GenderMap : MovieComponentMap<Gender>
    {
        public override void Configure(EntityTypeBuilder<Gender> builder)
        {
            base.Configure(builder);

            builder.HasMany(gender => gender.Movies)
                .WithMany(movie => movie.Genders);
        }
    }
}
