﻿using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Imdb.Infra.Mappings
{
    public abstract class MovieComponentMap<T> : EntityMap<T> where T : MovieComponent
    {
        public override void Configure(EntityTypeBuilder<T> builder)
        {
            base.Configure(builder);

            builder.Property(e => e.Name)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}
