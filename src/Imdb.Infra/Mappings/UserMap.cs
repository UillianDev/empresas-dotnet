﻿using Imdb.Core.Domain;
using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace Imdb.Infra.Mappings
{
    public class UserMap : EntityMap<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            base.Configure(builder);

            builder.Property(e => e.Name)
                .HasMaxLength(200)
                .IsRequired();

            builder.Property(e => e.IsAdministrator)
                .IsRequired();

            builder.OwnsOne(e => e.Email, email =>
            {
                email.Property(e => e.Value)
                    .HasMaxLength(320)
                    .IsRequired()
                    .HasColumnName("Email");

                email.HasData(new object[]
                {
                    new { new Email("administrator@email.com").Value, UserId = Guid.Parse("3CE5C261-95D9-4885-9549-69F39656B4F9") },
                    new { new Email("user@email.com").Value, UserId = Guid.Parse("494B2547-DADD-49BE-AA6F-D6E0305D4093") }
                });
            });

            builder.OwnsOne(x => x.Password, password =>
            {
                password.Property(x => x.Value)
                    .IsRequired()
                    .HasColumnName("Password");

                password.HasData(new object[]
                {
                    new { new Password("teste").Value, UserId = Guid.Parse("3CE5C261-95D9-4885-9549-69F39656B4F9") },
                    new { new Password("teste").Value, UserId = Guid.Parse("494B2547-DADD-49BE-AA6F-D6E0305D4093") }
                });
            });

            builder.HasMany(x => x.Votes)
                .WithOne(x => x.User)
                .IsRequired();

            builder.HasData(new object[]
            {
                new { IsActive = true, IsAdministrator = true, Name = "Administrator", Id = Guid.Parse("3CE5C261-95D9-4885-9549-69F39656B4F9"), Creation = new DateTime(2021, 4, 30) },
                new { IsActive = true, IsAdministrator = false, Name = "User", Id = Guid.Parse("494B2547-DADD-49BE-AA6F-D6E0305D4093"), Creation = new DateTime(2021, 4, 30) }
            });
        }
    }
}
