﻿using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Imdb.Infra.Mappings
{
    public class MovieMap : EntityMap<Movie>
    {
        public override void Configure(EntityTypeBuilder<Movie> builder)
        {

            builder.Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(x => x.Release)
                .IsRequired();

            builder.Property(x => x.Synopsis);

            builder.HasMany(x => x.Actors)
                .WithMany(x => x.Movies);

            builder.HasMany(x => x.Directors)
                .WithMany(x => x.Movies);

            builder.HasMany(x => x.Genders)
                .WithMany(x => x.Movies);

            builder.HasMany(x => x.Votes)
                .WithOne(x => x.Movie)
                .IsRequired();

            base.Configure(builder);
        }
    }
}
