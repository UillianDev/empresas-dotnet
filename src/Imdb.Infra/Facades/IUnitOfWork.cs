﻿using Imdb.Core.Facades;
using Imdb.Infra.Contexts;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Infra.Facades
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ImdbDbContext _dbContext;

        public UnitOfWork(ImdbDbContext dbContext) => _dbContext = dbContext;

        public Task CommitAsync(CancellationToken cancellationToken) => _dbContext.SaveChangesAsync(cancellationToken);
    }
}
