﻿using Libs.Cqrs.Contracts;
using Libs.Mediator.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Libs.Mediator.Implementations
{
    public class DefaultMediator : IMediator
    {
        private readonly IServiceProvider _serviceProvider;

        public DefaultMediator(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;

        public async Task<ICommandResponse> SendAsync<T>(T command, CancellationToken cancellationToken = default) where T : ICommandInput
        {
            var handler = _serviceProvider
                .GetRequiredService<ICommandHandler<T>>();

            if (await ExecuteInputInterceptorsAsync(command, cancellationToken) is ICommandResponse inputInterceptorResponse && !inputInterceptorResponse.Success)
                return inputInterceptorResponse;

            var output = await handler
                .HandleAsync(command, cancellationToken);

            if (await ExecuteOutputInterceptorsAsync(output, cancellationToken) is ICommandResponse ouputInterceptorsResponse && !ouputInterceptorsResponse.Success)
                return ouputInterceptorsResponse;

            return output;
        }

        private async Task<ICommandResponse> ExecuteOutputInterceptorsAsync<T>(T intercepted, CancellationToken cancellationToken) where T : ICommandResponse
        {
            var specificInteceptors = _serviceProvider
                .GetServices<ICommandOutputInterceptor<T>>();

            if (specificInteceptors != null && specificInteceptors.Any())
                foreach (var interceptor in specificInteceptors)
                    if (await interceptor.InterceptAsync(intercepted, cancellationToken) is ICommandResponse response && !response.Success)
                        return response;

            var genericInterceptors = _serviceProvider
                .GetServices<ICommandOutputInterceptor>();

            if (genericInterceptors != null && genericInterceptors.Any())
                foreach (var interceptor in genericInterceptors)
                    if (await interceptor.InterceptAsync(intercepted, cancellationToken) is ICommandResponse response && !response.Success)
                        return response;

            return intercepted;
        }

        private async Task<ICommandResponse> ExecuteInputInterceptorsAsync<T>(T intercepted, CancellationToken cancellationToken) where T : ICommandInput
        {
            var specificInteceptors = _serviceProvider
                .GetServices<ICommandInputInterceptor<T>>();

            if (specificInteceptors != null && specificInteceptors.Any())
                foreach (var interceptor in specificInteceptors)
                    if (await interceptor.InterceptAsync(intercepted, cancellationToken) is ICommandResponse response && !response.Success)
                        return response;

            var genericInterceptors = _serviceProvider
                .GetServices<ICommandInputInterceptor>();

            if (genericInterceptors != null && genericInterceptors.Any())
                foreach (var interceptor in genericInterceptors)
                    if (await interceptor.InterceptAsync(intercepted, cancellationToken) is ICommandResponse response && !response.Success)
                        return response;

            return null;

        }

    }
}
