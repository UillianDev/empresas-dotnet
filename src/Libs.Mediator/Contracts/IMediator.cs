﻿using Libs.Cqrs.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Libs.Mediator.Contracts
{
    public interface IMediator
    {
        Task<ICommandResponse> SendAsync<T>(T command, CancellationToken cancellationToken = default) where T : ICommandInput;
    }
}
