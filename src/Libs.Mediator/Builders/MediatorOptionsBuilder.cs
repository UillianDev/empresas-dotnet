﻿using Libs.Cqrs.Contracts;
using Libs.Mediator.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Libs.Mediator.Builders
{
    public class MediatorOptionsBuilder
    {
        private Type _queryHandlerType;
        private readonly List<(Type interfaceType, Type implementationType)> _commandHandlerTypes;
        private readonly List<(Type interfaceType, Type implementationType)> _interceptorTypes;

        public MediatorOptionsBuilder()
        {
            _commandHandlerTypes = new List<(Type interfaceType, Type implementationType)>();
            _interceptorTypes = new List<(Type interfaceType, Type implementationType)>();
        }

        public MediatorOptionsBuilder FromAssemblyOf<T>()
        {
            var handlerTypes = typeof(T).Assembly.GetTypes()
                .Where(x => x.GetInterfaces() is Type[] ifs &&
                  ifs.Any(y => y.GetInterfaces() is Type[] yifs &&
                      yifs.Contains(typeof(ICommandHandler))));

            foreach (var handlerType in handlerTypes)
                foreach (var interfaceType in handlerType.GetInterfaces())
                    _commandHandlerTypes.Add((interfaceType, handlerType));

             return this;
        }

        public MediatorOptionsBuilder AddInterceptor<T, R>() where T : ICommandInputInterceptor<R> where R : ICommandInput
        {
            _interceptorTypes.Add((typeof(ICommandInputInterceptor<R>), typeof(T)));
            return this;
        }

        public MediatorOptionsBuilder AddInterceptor<T>() where T : ICommandInputInterceptor
        {
            _interceptorTypes.Add((typeof(ICommandInputInterceptor), typeof(T)));
            return this;
        }

        public MediatorOptionsBuilder AddOutputInterceptor<T, R>() where T : ICommandOutputInterceptor<R> where R : ICommandResponse
        {
            _interceptorTypes.Add((typeof(ICommandOutputInterceptor<R>), typeof(T)));
            return this;
        }

        public MediatorOptionsBuilder AddOutputInterceptor<T>() where T : ICommandOutputInterceptor
        {
            _interceptorTypes.Add((typeof(ICommandOutputInterceptor), typeof(T)));
            return this;
        }

        public MediatorOptionsBuilder AddQueryHandler<T>() where T : IQueryHandler
        {
            _queryHandlerType = typeof(T);
            return this;
        }

        public MediatorOptions Build() => new MediatorOptions
        {
            Interceptors = _interceptorTypes,
            QueryType = _queryHandlerType,
            CommandHandlers = _commandHandlerTypes
        };
    }
}
