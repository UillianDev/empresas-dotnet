﻿using Libs.Cqrs.Contracts;
using Libs.Mediator.Builders;
using Libs.Mediator.Contracts;
using Libs.Mediator.Implementations;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static void AddMediator(this IServiceCollection services, Action<MediatorOptionsBuilder> configureOptions)
        {
            var builder = new MediatorOptionsBuilder();
            
            configureOptions(builder);

            var options = builder.Build();

            foreach (var (interfaceType, implementationType) in options.CommandHandlers)
                services.AddScoped(interfaceType, implementationType);

            foreach (var (interfaceType, implementationType) in options.Interceptors)
                services.AddScoped(interfaceType, implementationType);

            if (options.QueryType is Type queryType)
                services.AddScoped(typeof(IQueryHandler), queryType);

            services.AddScoped<IMediator, DefaultMediator>();
        }
    }
}
