﻿using System;
using System.Collections.Generic;

namespace Libs.Mediator.Options
{
    public class MediatorOptions
    {
        public Type QueryType { get; set; }
        public List<(Type interfaceType, Type implementationType)> Interceptors { get; set; }
        public List<(Type interfaceType, Type implementationType)> CommandHandlers { get; set; }
    }
}
