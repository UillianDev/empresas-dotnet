﻿using System.Threading;
using System.Threading.Tasks;

namespace Libs.Cqrs.Contracts
{
    public interface ICommandInterceptor<T>
    {
        Task<ICommandResponse> InterceptAsync<C>(C intercepted, CancellationToken cancellationToken = default) where C : T;
    }

    public interface ICommandInputInterceptor : ICommandInterceptor<ICommandInput>
    {
    }

    public interface ICommandOutputInterceptor : ICommandInterceptor<ICommandResponse>
    {
    }

    public interface ICommandInputInterceptor<T> : ICommandInterceptor<T> where T : ICommandInput
    {
    }

    public interface ICommandOutputInterceptor<T> : ICommandInterceptor<T> where T : ICommandResponse
    {
    }


}
