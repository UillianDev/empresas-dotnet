﻿using System.Linq;

namespace Libs.Cqrs.Contracts
{
    public interface IQueryHandler
    {
        IQueryable<R> Handle<T, R>(IQuery<T, R> query) where T : class where R : class;

        IQueryable<T> Handle<T>(IQuery<T> query) where T : class;
    }
}
