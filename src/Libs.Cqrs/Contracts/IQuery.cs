﻿using System.Linq;
using System.Threading;

namespace Libs.Cqrs.Contracts
{
    public interface IQuery<T> where T : class
    {
        public IQueryable<T> Query(IQueryable<T> query);
    }

    public interface IQuery<T, R> where T : class where R : class
    {
        public IQueryable<R> Query(IQueryable<T> query);
    }
}
