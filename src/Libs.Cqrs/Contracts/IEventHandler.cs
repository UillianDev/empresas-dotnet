﻿using System.Threading;
using System.Threading.Tasks;

namespace Libs.Cqrs.Contracts
{
    public interface IEventHandler { }

    public interface IEventHandler<T> where T : IEvent
    {
        Task PublishAsync(T someEvent, CancellationToken cancellationToken);
    }
}
