﻿namespace Libs.Cqrs.Contracts
{
    public interface ICommandResponse
    {
        bool Success { get; }
        object Data { get; }
        string[] Messages { get; }
    }
}
