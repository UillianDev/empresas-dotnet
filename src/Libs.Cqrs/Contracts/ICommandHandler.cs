﻿using System.Threading;
using System.Threading.Tasks;

namespace Libs.Cqrs.Contracts
{
    public interface ICommandHandler { }

    public interface ICommandHandler<T> : ICommandHandler where T : ICommandInput
    {
        Task<ICommandResponse> HandleAsync(T command, CancellationToken cancellationToken);
    }
}
