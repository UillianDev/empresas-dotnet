﻿using Imdb.Core.Domain;
using Imdb.Core.Utils;
using System.Collections.Generic;
using System.Security.Claims;

namespace Imdb.Domain.Entities
{
    public class User : Entity
    {
        protected User() : base()
        {
            Votes = new List<Vote>();
        }

        private User(string name, string email, string password, bool administrator) : this()
        {
            Name = name;
            Email = new Email(email);
            Password = new Password(password);
            IsAdministrator = administrator;
        }

        public User(string name, string email, string password) : this(name, email, password, false) { }

        public static User Administrator(string name, string email, string password) => new User(name, email, password, true);

        public string Name { get; private set; }
        public virtual Email Email { get; private set; }
        public virtual Password Password { get; private set; }
        public bool IsAdministrator { get; private set; }

        public virtual ICollection<Vote> Votes { get; }

        public void ChangeName(string name)
        {
            Name = name;
        }

        public void ChangeEmail(string email)
        {
            Email = new Email(email);
        }

        public void ChangePassword(string password)
        {
            Password = new Password(password);
        }

        public void Deactivate()
        {
            IsActive = false;
        }

        public IEnumerable<Claim> GetClaims()
        {

            yield return new Claim(ClaimTypes.Name, Name);
            yield return new Claim(ClaimTypes.Email, Email.Value);
            yield return new Claim(ClaimTypes.NameIdentifier, Id.ToString());

            if (IsAdministrator)
                yield return new Claim(ClaimTypes.Role, ImdbRoles.Administrator);
            else
                yield return new Claim(ClaimTypes.Role, ImdbRoles.User);

        }
    }
}
