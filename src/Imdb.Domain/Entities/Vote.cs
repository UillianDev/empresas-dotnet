﻿using Imdb.Core.Domain;

namespace Imdb.Domain.Entities
{
    public class Vote : Entity
    {
        protected Vote() : base()
        {
        }

        public Vote(User user, Movie movie, int score) : this()
        {
            User = user;
            Movie = movie;
            Score = score;
        }

        public virtual User User { get; private set; }
        public virtual Movie Movie { get; private set; }
        public int Score { get; private set; }

        public void ChangeScore(int score)
        {
            Score = score;
        }
    }
}
