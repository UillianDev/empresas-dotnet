﻿using Imdb.Core.Domain;
using System.Collections.Generic;

namespace Imdb.Domain.Entities
{
    public abstract class MovieComponent : Entity
    {
        protected MovieComponent() : base() => Movies = new HashSet<Movie>();

        public MovieComponent(string name) : this() => Name = name;

        public string Name { get; set; }
        public virtual ISet<Movie> Movies { get; }
    }
}
