﻿using Imdb.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Imdb.Domain.Entities
{
    public class Movie : Entity
    {
        protected Movie() : base()
        {
            Votes = new HashSet<Vote>();
            Genders = new HashSet<Gender>();
            Directors = new HashSet<Director>();
            Actors = new HashSet<Actor>();
        }

        public Movie(string name, string synopsis, DateTime release,
            ISet<Gender> genders, ISet<Actor> actors,
            ISet<Director> directors)
        {
            Name = name;
            Release = release;
            Synopsis = synopsis;

            foreach (var actor in actors)
                Add(actor);

            foreach (var gender in genders)
                Add(gender);

            foreach (var director in directors)
                Add(director);
        }

        public string Name { get; private set; }
        public string Synopsis { get; private set; }
        public DateTime Release { get; private set; }

        public virtual ISet<Vote> Votes { get; }
        public virtual ISet<Gender> Genders { get; }
        public virtual ISet<Actor> Actors { get; }
        public virtual ISet<Director> Directors { get; }

        public void Add(Gender gender)
        {
            if (Genders.Any(g => g.Id == gender.Id))
                return;

            Genders.Add(gender);
        }

        public void Add(Actor actor)
        {
            if (Actors.Any(a => a.Id == actor.Id))
                return;

            Actors.Add(actor);
        }

        public void Add(Director director)
        {
            if (Directors.Any(d => d.Id == director.Id))
                return;

            Directors.Add(director);
        }

        public void Remove(Gender gender)
        {
            if (!Genders.Any(g => g.Id == gender.Id))
                return;

            Genders.Remove(gender);
        }

        public void Remove(Actor actor)
        {
            if (!Actors.Any(a => a.Id == actor.Id))
                return;

            Actors.Remove(actor);
        }

        public void Remove(Director director)
        {
            if (Directors.Any(d => d.Id == director.Id))
                return;

            Directors.Add(director);
        }

        public void Vote(int score, User user)
        {
            if (Votes.FirstOrDefault(vote => vote.User == user) is Vote vote)
            {
                vote.ChangeScore(score);
                return;
            }

            vote = new Vote(user, this, score);
            Votes.Add(vote);
            
        }
    }
}
