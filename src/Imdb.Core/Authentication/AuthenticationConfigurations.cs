﻿namespace Imdb.Core.Authentication
{
    public class AuthenticationConfigurations
    {
        public const string CryptKey = "4E3BD8C7-7824-4E33-8337-0EA2C14ADA17";
        public const string Audience = "prosystem.whatsapp.watcher.audience";
        public const string Issuer = "prosystem.whatsapp.watcher.issuer";
    }
}
