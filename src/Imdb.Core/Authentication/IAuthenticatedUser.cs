﻿using System;

namespace Imdb.Core.Authentication
{
    public interface IAuthenticatedUser
    {
        public Guid Id { get; }
        public string Name { get; }
        public string Email { get; }

    }
}
