﻿using Imdb.Core.Domain;
using System;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Core.Repositories
{
    public interface IRepository
    {
        Task AddAsync<T>(T entity, CancellationToken cancellationToken) where T : Entity;

        void Update<T>(T entity) where T : Entity;

        void Delete<T>(T entity) where T : Entity;

        Task<T> GetByIdAsync<T>(Guid id, CancellationToken cancellationToken) where T : Entity;

        Task<T> GetFirstOrDefaultAsync<T>(Expression<Func<T, bool>> expression, CancellationToken cancellationToken) where T : Entity;

        Task<bool> AnyAsync<T>(Expression<Func<T, bool>> expression, CancellationToken cancellationToken) where T : Entity;
    }
}
