﻿using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Core.Facades
{
    public interface IUnitOfWork
    {
        Task CommitAsync(CancellationToken cancellationToken);
    }
}
