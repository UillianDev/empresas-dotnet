﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Imdb.Core.Domain
{
    public class DomainAssertions
    {
        private static void Assert(Func<string> assertion)
        {
            var message = assertion();

            if (string.IsNullOrEmpty(message))
                return;

            throw new DomainException(message);
        }
        
        private static bool ValidateEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                static string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    string domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
            catch (ArgumentException)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        public static void IsBetween<T>(T target, T start, T end, string message) where T : IComparable => Assert(() => target.CompareTo(start) >= 0 && target.CompareTo(end) <= 0 ? null : message);

        public static void IsNotNull<T>(T target, string message) where T : IComparable => Assert(() => target != null ? null : message);

        public static void IsNotEmpty(string target, string message) => Assert(() => target != null ? null : message);

        public static void IsEmail(string target, string message) => Assert(() => ValidateEmail(target) ? null : message);


    }

    public class DomainException : Exception
    {
        public DomainException(string message) : base(message) { }
    }
}
