﻿namespace Imdb.Core.Domain
{
    public class Email
    {
        public string Value { get; set; }

        protected Email() { }

        public Email(string value) : this()
        {
            DomainAssertions.IsNotNull(value, "Invalid e-mail address");
            DomainAssertions.IsNotEmpty(value, "Invalid e-mail address");
            DomainAssertions.IsEmail(value, "Invalid e-mail address");

            Value = value;
        }
    }
}
