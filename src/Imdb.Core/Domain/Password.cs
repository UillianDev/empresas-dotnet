﻿using System.Security.Cryptography;
using System.Text;

namespace Imdb.Core.Domain
{
    public class Password
    {
        public string Value { get; set; }

        protected Password() { }

        public Password(string value)
        {
            Value = Encrypt(value);

        }

        public static string Encrypt(string value)
        {
            var hash = SHA256.Create();
            var utf8Value = Encoding.UTF8.GetBytes(value);
            var encryptedValue = hash.ComputeHash(utf8Value);
            var stringBuilder = new StringBuilder();

            foreach (var character in encryptedValue)
                stringBuilder.Append(character.ToString("X2"));

            return stringBuilder.ToString();
        }
    }
}
