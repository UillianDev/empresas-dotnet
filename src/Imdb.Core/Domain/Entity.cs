﻿using System;

namespace Imdb.Core.Domain
{
    public abstract class Entity
    {
        public Guid Id { get; set; }
        public DateTime Creation { get; set; }
        public bool IsActive { get; set; }

        //private List<Event> _events;
        //public virtual IReadOnlyCollection<Event> Events => _events.AsReadOnly();

        protected Entity()
        {
            Id = Guid.NewGuid();
            Creation = DateTime.Now;
            IsActive = true;
        }

        //public void AddEvent(Event message)
        //{
        //    _events ??= new List<Event>();
        //    _events.Add(message);
        //}
    }
}
