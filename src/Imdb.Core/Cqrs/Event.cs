﻿using Libs.Cqrs.Contracts;
using System;

namespace Imdb.Core.Messages
{
    public abstract class Event : Message, IEvent
    {
        public Guid AggregateId { get; private set; }

        public Event() : base() { }

        public Event(Guid aggregateId) : this()
        {
            AggregateId = aggregateId;
        }
    }
}
