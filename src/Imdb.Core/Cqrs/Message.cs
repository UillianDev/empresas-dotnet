﻿using System;
using System.Text.Json.Serialization;

namespace Imdb.Core.Messages
{
    public abstract class Message
    {
        [JsonIgnore]
        public string CommanType { get; private set; }

        [JsonIgnore]
        public DateTime Timestamp { get; private set; }

        protected Message()
        {
            Timestamp = DateTime.Now;
            CommanType = GetType().FullName;
        }
    }
}
