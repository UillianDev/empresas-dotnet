﻿using Libs.Cqrs.Contracts;

namespace Imdb.Core.Messages
{
    public abstract class Command : Message, ICommandInput
    {
        public Command() : base() { }
    }
}
