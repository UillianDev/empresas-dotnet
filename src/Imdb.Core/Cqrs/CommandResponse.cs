﻿

using Libs.Cqrs.Contracts;

namespace Imdb.Core.Cqrs
{
    public class CommandResponse : ICommandResponse
    {
        private CommandResponse(bool success, object data, string[] messages)
        {
            Success = success;
            Messages = messages;
            Data = data;
        }

        public bool Success { get; set; }
        public string[] Messages { get; set; }
        public object Data { get; set; }

        public static ICommandResponse Ok(object data, params string[] messages) => new CommandResponse(true, data, messages);
        public static ICommandResponse Ok(params string[] messages) => Ok(null, messages);

        public static ICommandResponse Failure(object data, params string[] messages) => new CommandResponse(false, data, messages);
        public static ICommandResponse Failure(params string[] messages) => Failure(null, messages);

        public static ICommandResponse Nothing() => null;
    }
}
