﻿namespace Imdb.Application.Queries.Responses.Movies
{
    public class MovieDetailItem : MovieListItem
    {
        public string[] Directors { get; set; }
        public string[] Actors { get; set; }
        public string[] Genders { get; set; }
    }
}
