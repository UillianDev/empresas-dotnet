﻿using System;
using System.Text.Json.Serialization;

namespace Imdb.Application.Queries.Responses.Movies
{
    public class MovieListItem
    {
        public Guid Id { get; set; }
        public DateTime Release { get; set; }
        public string Name { get; set; }
        public int Votes { get; set; }
        public string Synopsis { get; set; }
        public decimal AverageScore { get => Votes > 0 ? TotalScore / Votes : 0.00m; }

        [JsonIgnore]
        public decimal TotalScore { get; set; }
        
    }
}
