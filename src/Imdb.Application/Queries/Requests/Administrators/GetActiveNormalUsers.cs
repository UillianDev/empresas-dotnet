﻿using Imdb.Application.Queries.Requests.Users;
using Imdb.Application.Queries.Responses.Users;
using Imdb.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Imdb.Application.Queries.Requests.Administrator
{
    public class GetActiveNormalUsers : UserQuery
    {
        public string Filter { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }

        public override IQueryable<UserItem> Query(IQueryable<User> query)
        {
            query = query
                .Where(user => user.IsActive && !user.IsAdministrator)
                .OrderBy(user => user.Name);

            if (!string.IsNullOrEmpty(Filter))
                query = query.Where(user =>
                    EF.Functions.Like(user.Name, $"{Filter}%") ||
                    EF.Functions.Like(user.Email.Value, $"{Filter}%")
                );

            if (Page is int page && page > 0)
            {
                var pageSize = PageSize ?? 50;
                query = query.Skip(page - 1).Take(pageSize);
            }

            return base.Query(query);
        }
    }
}
