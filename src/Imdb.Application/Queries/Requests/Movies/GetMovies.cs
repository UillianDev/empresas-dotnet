﻿using Imdb.Application.Queries.Responses.Movies;
using Imdb.Domain.Entities;
using Libs.Cqrs.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Imdb.Application.Queries.Requests.Movies
{
    public class GetMovies : IQuery<Movie, MovieListItem>
    {
        public string Name { get; set; }
        public ICollection<Guid> Directors { get; set; }
        public ICollection<Guid> Actors { get; set; }
        public ICollection<Guid> Genders { get; set; }
        public int? PageSize { get; set; }
        public int? Page { get; set; }

        public IQueryable<MovieListItem> Query(IQueryable<Movie> query)
        {
            if (Directors is ICollection<Guid> directors && directors.Any())
                query = query.Where(movie => movie.Directors.Any(director => directors.Contains(director.Id)));

            if (Actors is ICollection<Guid> actors && actors.Any())
                query = query.Where(movie => movie.Actors.Any(actor => actors.Contains(actor.Id))); ;

            if (Genders is ICollection<Guid> genders && genders.Any())
                query = query.Where(movie => movie.Genders.Any(gender => genders.Contains(gender.Id))); ;

            if (!string.IsNullOrEmpty(Name))
                query = query.Where(movie => EF.Functions.Like(movie.Name, $"{Name}%"));

            if (Page is int page && page > 0)
            {
                var pageSize = PageSize ?? 50;
                query = query.Skip(page - 1).Take(pageSize);
            }

            return query
                .Where(movie => movie.IsActive)
                .OrderByDescending(movie => movie.Votes.Count)
                .ThenBy(movie => movie.Name)
                .Select(movie => new MovieListItem
                {
                    Id = movie.Id,
                    Release = movie.Release,
                    Name = movie.Name,
                    Votes = movie.Votes.Count,
                    Synopsis = movie.Synopsis,
                    TotalScore = movie.Votes.Sum(vote => vote.Score)
                });
        }
    }
}
