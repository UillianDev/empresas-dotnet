﻿using Imdb.Application.Queries.Responses.Movies;
using Imdb.Domain.Entities;
using Libs.Cqrs.Contracts;
using System;
using System.Linq;

namespace Imdb.Application.Queries.Requests.Movies
{
    public class GetMovieDatails : IQuery<Movie, MovieDetailItem>
    {
        public GetMovieDatails(Guid movieId)
        {
            MovieId = movieId;
        }

        public Guid MovieId { get; set; }

        public IQueryable<MovieDetailItem> Query(IQueryable<Movie> query) => query
            .Where(movie => movie.Id == MovieId && movie.IsActive)
            .Select(movie => new MovieDetailItem
            {
                Id = movie.Id,
                Release = movie.Release,
                Name = movie.Name,
                Directors = movie.Directors.Select(e => e.Name).ToArray(),
                Actors = movie.Actors.Select(e => e.Name).ToArray(),
                Genders = movie.Genders.Select(e => e.Name).ToArray(),
                Votes = movie.Votes.Count,
                TotalScore = movie.Votes.Sum(vote => vote.Score)
            });
    }
}
