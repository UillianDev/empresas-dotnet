﻿using Imdb.Application.Queries.Responses.Users;
using Imdb.Domain.Entities;
using System;
using System.Linq;

namespace Imdb.Application.Queries.Requests.Users
{
    public class GetUserDetails : UserQuery
    {
        public Guid Id { get; set; }

        public override IQueryable<UserItem> Query(IQueryable<User> query) => base.Query(query.Where(e => e.Id == Id));
    }
}
