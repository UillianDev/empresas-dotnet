﻿using Imdb.Application.Queries.Responses.Users;
using Imdb.Domain.Entities;
using Libs.Cqrs.Contracts;
using System.Linq;

namespace Imdb.Application.Queries.Requests.Users
{
    public class UserQuery : IQuery<User, UserItem>
    {
        public virtual IQueryable<UserItem> Query(IQueryable<User> query) => query
            .Select(user => new UserItem
            {
                Id = user.Id,
                Name = user.Name,
                Email = user.Email.Value
            });
    }
}
