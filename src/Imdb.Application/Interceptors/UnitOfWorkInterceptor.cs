﻿using Imdb.Core.Facades;
using Libs.Cqrs.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Application.Interceptors
{
    public class UnitOfWorkInterceptor : ICommandOutputInterceptor
    {
        private readonly IUnitOfWork _unitOfWork;

        public UnitOfWorkInterceptor(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<ICommandResponse> InterceptAsync<C>(C intercepted, CancellationToken cancellationToken = default) where C : ICommandResponse
        {
            if (!intercepted.Success)
                return intercepted;

            await _unitOfWork
                .CommitAsync(cancellationToken);

            return intercepted;
        }
    }
}
