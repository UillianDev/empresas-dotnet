﻿using FluentValidation;
using Imdb.Core.Cqrs;
using Libs.Cqrs.Contracts;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Application.Interceptors
{
    public class ValidatorInterceptor : ICommandInputInterceptor
    {
        private readonly IServiceProvider _serviceProvider;

        public ValidatorInterceptor(IServiceProvider serviceProvider) => _serviceProvider = serviceProvider;

        public Task<ICommandResponse> InterceptAsync<C>(C intercepted, CancellationToken cancellationToken = default) where C : ICommandInput
        {
            var validator = _serviceProvider
                .GetService<IValidator<C>>();

            if (validator == null)
                return Task.FromResult(CommandResponse.Nothing());

            var result = validator.Validate(intercepted);

            if (result.IsValid)
                return Task.FromResult(CommandResponse.Nothing());

            var erros = result.Errors?
                .Select(er => er.ErrorMessage)?
                .ToArray() ?? Array.Empty<string>();

            return Task.FromResult(CommandResponse.Failure(erros));
        }
    }
}
