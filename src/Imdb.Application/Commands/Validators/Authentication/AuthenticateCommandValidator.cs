﻿using FluentValidation;
using Imdb.Application.Commands.Requests.Authentication;

namespace Imdb.Application.Commands.Validators.Authentication
{
    public class AuthenticateCommandValidator : AbstractValidator<AuthenticateCommand>
    {
        public AuthenticateCommandValidator()
        {
            RuleFor(e => e.Password)
                .MinimumLength(5)
                .WithMessage("The minimum password lenght is 5 characters")
                .NotEmpty()
                .NotNull()
                .WithMessage("Invalid password");

            RuleFor(e => e.Email)
               .MaximumLength(320)
               .WithMessage("The maximun user e-mail address length is 320 characters")
               .NotEmpty()
               .NotNull()
               .EmailAddress()
               .WithMessage("Invalid user e-mail address");
        }
    }
}
