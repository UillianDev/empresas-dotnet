﻿using FluentValidation;
using Imdb.Application.Commands.Requests.Movie;

namespace Imdb.Application.Commands.Validators.Movie
{
    public class VoteCommandValidator : AbstractValidator<VoteCommand>
    {
        public VoteCommandValidator()
        {
            RuleFor(movie => movie.Score)
                .InclusiveBetween(0, 4)
                .WithMessage("The movie score must be between 0 and 4");
        }
    }
}
