﻿using FluentValidation;
using Imdb.Application.Commands.Requests.Movie;

namespace Imdb.Application.Commands.Validators.Movie
{
    public class RegisterMovieCommandValidator : AbstractValidator<RegisterMovieCommand>
    {
        public RegisterMovieCommandValidator()
        {
            RuleFor(movie => movie.Name)
                .NotEmpty()
                .NotNull()
                .WithMessage("Invalid movie name")
                .MaximumLength(200)
                .WithMessage("The maximum lenght of a movie name is 200 characters");
        }
    }
}
