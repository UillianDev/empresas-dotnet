﻿using FluentValidation;
using Imdb.Application.Commands.Requests.User;
using System;

namespace Imdb.Application.Commands.Validators.User
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator() : base()
        {
            RuleFor(c => c.Id)
                .Must(id => id != Guid.Empty)
                .WithMessage("Invalid id");
        }
    }
}
