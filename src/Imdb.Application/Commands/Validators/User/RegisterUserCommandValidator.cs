﻿using FluentValidation;
using Imdb.Application.Commands.Requests.User;

namespace Imdb.Application.Commands.Validators.User
{
    public class RegisterUserCommandValidator : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidator() : base()
        {
            RuleFor(e => e.Name)
                .MaximumLength(320)
                .WithMessage("The maximum user name length is 200 charecters")
                .NotEmpty()
                .NotNull()
                .WithMessage("Invalid user name");

            RuleFor(e => e.Password)
                .MinimumLength(8)
                .WithMessage("The minimum password lenght is 8 characters")
                .NotEmpty()
                .NotNull()
                .WithMessage("Invalid password");

            RuleFor(e => e.Email)
               .MaximumLength(320)
               .WithMessage("The maximun user e-mail address length is 320 characters")
               .NotEmpty()
               .NotNull()
               .EmailAddress()
               .WithMessage("Invalid user e-mail address");
        }
    }
}
