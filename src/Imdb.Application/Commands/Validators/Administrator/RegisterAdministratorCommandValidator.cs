﻿using FluentValidation;
using Imdb.Application.Commands.Requests.Administrator;

namespace Imdb.Application.Commands.Validators.Administrator
{
    public class RegisterAdministratorCommandValidator : AbstractValidator<RegisterAdministratorCommand>
    {
        public RegisterAdministratorCommandValidator() : base()
        {
            RuleFor(e => e.Name)
                .MaximumLength(320)
                .WithMessage("The maximum administrator name length is 200 charecters")
                .NotEmpty()
                .NotNull()
                .WithMessage("Invalid administrator name");

            RuleFor(e => e.Password)
                .MinimumLength(8)
                .WithMessage("The minimum passaword lenght is 8 characters")
                .NotEmpty()
                .NotNull()
                .WithMessage("Invalid password");

            RuleFor(e => e.Email)
               .MaximumLength(320)
               .WithMessage("The maximun e-mail address length is 320 characters")
               .NotEmpty()
               .NotNull()
               .EmailAddress()
               .WithMessage("Invalid e-mail address");
        }
    }
}
