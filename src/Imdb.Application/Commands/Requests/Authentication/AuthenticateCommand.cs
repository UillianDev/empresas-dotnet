﻿using Imdb.Core.Messages;

namespace Imdb.Application.Commands.Requests.Authentication
{
    public class AuthenticateCommand : Command
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
