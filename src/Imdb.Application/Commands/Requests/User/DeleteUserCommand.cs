﻿using Imdb.Core.Messages;
using System;

namespace Imdb.Application.Commands.Requests.User
{
    public class DeleteUserCommand : Command
    {
        public DeleteUserCommand(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; set; }

    }
}
