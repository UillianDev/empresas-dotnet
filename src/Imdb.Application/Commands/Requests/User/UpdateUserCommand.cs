﻿using Imdb.Core.Messages;
using System;

namespace Imdb.Application.Commands.Requests.User
{
    public class UpdateUserCommand : Command
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
