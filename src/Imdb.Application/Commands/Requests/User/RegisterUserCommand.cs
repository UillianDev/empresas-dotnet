﻿using Imdb.Core.Messages;

namespace Imdb.Application.Commands.Requests.User
{
    public class RegisterUserCommand : Command
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        
    }
}
