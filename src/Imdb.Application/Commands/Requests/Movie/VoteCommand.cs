﻿using Imdb.Core.Messages;
using System;
using System.Text.Json.Serialization;

namespace Imdb.Application.Commands.Requests.Movie
{

    public class VoteCommand : Command
    {
        [JsonIgnore]
        public Guid MovieId { get; set; }

        public int Score { get; set; }
    }
}
