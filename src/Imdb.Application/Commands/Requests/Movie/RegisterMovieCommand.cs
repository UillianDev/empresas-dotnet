﻿using Imdb.Core.Messages;
using System;
using System.Collections.Generic;

namespace Imdb.Application.Commands.Requests.Movie
{
    public class RegisterMovieCommand : Command
    {
        public string Name { get; set; }
        public string Synopsis { get; set; }
        public DateTime Release { get; set; }
        public List<Guid> Genders { get; set; } = new List<Guid>();
        public List<Guid> Actors { get; set; } = new List<Guid>();
        public List<Guid> Directors { get; set; } = new List<Guid>();
    }
}
