﻿using Imdb.Application.Commands.Requests.User;
using Imdb.Application.Queries.Responses.Users;
using Imdb.Core.Cqrs;
using Imdb.Core.Repositories;
using Imdb.Domain.Entities;
using Libs.Cqrs.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Application.Handlers
{
    public class UserHandler : 
        ICommandHandler<RegisterUserCommand>, 
        ICommandHandler<UpdateUserCommand>,
        ICommandHandler<DeleteUserCommand>
    {
        private readonly IRepository _repository;

        public UserHandler(IRepository repository)
        {
            _repository = repository;
        }

        public async Task<ICommandResponse> HandleAsync(RegisterUserCommand command, CancellationToken cancellationToken)
        {
            if (await _repository.AnyAsync<User>(e => e.Email.Value == command.Email, cancellationToken))
                return CommandResponse.Failure($"The email {command.Email} is already in use");

            var user = new User(command.Name, command.Email, command.Password);

            await _repository.AddAsync(user, cancellationToken);

            return CommandResponse.Ok(new UserItem 
            {
                Email = user.Email.Value,
                Id = user.Id,
                Name = user.Name
            }, 
            "The user was added with success");
        }

        public async Task<ICommandResponse> HandleAsync(DeleteUserCommand command, CancellationToken cancellationToken)
        {
            if (await _repository.GetByIdAsync<User>(command.Id, cancellationToken) is not User user)
                return CommandResponse.Failure($"No user found with this id");

            user.Deactivate();

            return CommandResponse.Ok("User deleted with success");
        }

        public async Task<ICommandResponse> HandleAsync(UpdateUserCommand command, CancellationToken cancellationToken)
        {
            if (await _repository.GetByIdAsync<User>(command.Id, cancellationToken) is not User user)
                return CommandResponse.Failure($"No user found with this id");

            user.ChangeName(command.Name);

            return CommandResponse.Ok("User deleted with success");
        }
    }
}
