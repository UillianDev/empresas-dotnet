﻿using Imdb.Application.Commands.Requests.Authentication;
using Imdb.Core.Authentication;
using Imdb.Core.Cqrs;
using Imdb.Core.Domain;
using Imdb.Core.Repositories;
using Imdb.Domain.Entities;
using Libs.Cqrs.Contracts;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Application.Handlers
{
    public class AuthenticationHandler : ICommandHandler<AuthenticateCommand>
    {
        private readonly IRepository _repository;

        public AuthenticationHandler(IRepository repository) => _repository = repository;

        public async Task<ICommandResponse> HandleAsync(AuthenticateCommand command, CancellationToken cancellationToken)
        {
            if (await _repository.GetFirstOrDefaultAsync<User>(user => user.Email.Value == command.Email, cancellationToken) is not User user)
                return CommandResponse.Failure("Incorrect email or password");

            if (user.Password == new Password(command.CommanType))
                return CommandResponse.Failure("Incorrect email or password");

            var manipuladorJwt = new JwtSecurityTokenHandler();
            var symmetricKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AuthenticationConfigurations.CryptKey));
            var cretentials = new SigningCredentials(symmetricKey, SecurityAlgorithms.HmacSha256Signature);
            var expiration = DateTime.UtcNow.AddHours(8);

            var descritorToken = new SecurityTokenDescriptor
            {
                Issuer = AuthenticationConfigurations.Issuer,
                Audience = AuthenticationConfigurations.Audience,
                Subject = new ClaimsIdentity(user.GetClaims()),
                Expires = expiration,
                SigningCredentials = cretentials
            };

            var seguracaToken = manipuladorJwt
                .CreateJwtSecurityToken(descritorToken);

            var token = manipuladorJwt
                .WriteToken(seguracaToken);

            return CommandResponse.Ok(new
            {
                Expiration = expiration.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds,
                Value = token
            }, "Authenticated with success");
        }
    }
}
