﻿using Imdb.Application.Commands.Requests.Movie;
using Imdb.Core.Authentication;
using Imdb.Core.Cqrs;
using Imdb.Core.Repositories;
using Imdb.Domain.Entities;
using Libs.Cqrs.Contracts;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Application.Handlers
{
    public class MovieHandler : ICommandHandler<RegisterMovieCommand>, ICommandHandler<VoteCommand>
    {
        private readonly IRepository _repository;
        private readonly IAuthenticatedUser _authenticatedUser;

        public MovieHandler(IRepository repository, IAuthenticatedUser authenticatedUser)
        {
            _repository = repository;
            _authenticatedUser = authenticatedUser;
        }

        public async Task<ICommandResponse> HandleAsync(RegisterMovieCommand command, CancellationToken cancellationToken)
        {
            var genders = new HashSet<Gender>();
            foreach(var genderId in command.Genders)
            {
                var gender = await _repository.GetByIdAsync<Gender>(genderId, cancellationToken);

                if (gender is not Gender)
                    continue;

                genders.Add(gender);
            }

            var directors = new HashSet<Director>();
            foreach (var directorId in command.Directors)
            {
                var director = await _repository.GetByIdAsync<Director>(directorId, cancellationToken);

                if (director is not Director)
                    continue;

                directors.Add(director);
            }

            var actors = new HashSet<Actor>();
            foreach (var actorId in command.Actors)
            {
                var actor = await _repository
                    .GetByIdAsync<Actor>(actorId, cancellationToken);

                if (actor is not Actor)
                    continue;

                actors.Add(actor);
            }

            var movie = new Movie(command.Name, command.Synopsis, command.Release, genders, actors, directors);

            await _repository.AddAsync(movie, cancellationToken);

            return CommandResponse.Ok("Movie added with success!");
        }

        public async Task<ICommandResponse> HandleAsync(VoteCommand command, CancellationToken cancellationToken)
        {
            if (await _repository.GetByIdAsync<User>(_authenticatedUser.Id, cancellationToken) is not User user)
                return CommandResponse.Failure("Invalid authenticated user, please try login again!");

            if (await _repository.GetByIdAsync<Movie>(command.MovieId, cancellationToken) is not Movie movie)
                return CommandResponse.Failure("Movie not found!");

            movie.Vote(command.Score, user);

            return CommandResponse.Ok("Vote computed with success!");
        }
    }
}
