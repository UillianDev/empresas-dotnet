﻿using Imdb.Application.Commands.Requests.Administrator;
using Imdb.Application.Queries.Responses.Users;
using Imdb.Core.Cqrs;
using Imdb.Core.Repositories;
using Imdb.Domain.Entities;
using Libs.Cqrs.Contracts;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Application.Handlers
{
    public class AdministratorHandler : 
        ICommandHandler<RegisterAdministratorCommand>
    {
        private readonly IRepository _repository;

        public AdministratorHandler(IRepository repository) => _repository = repository;

        public async Task<ICommandResponse> HandleAsync(RegisterAdministratorCommand command, CancellationToken cancellationToken)
        {
            if (await _repository.AnyAsync<User>(e => e.Email.Value == command.Email, cancellationToken))
                return CommandResponse.Failure($"The email {command.Email} is already in use");

            var administrator = User.Administrator(command.Name, command.Email, command.Password);

            await _repository.AddAsync(administrator, cancellationToken);

            return CommandResponse.Ok(new UserItem
            {
                Email = administrator.Email.Value,
                Id = administrator.Id,
                Name = administrator.Name
            },
            "The administrator was added with success");
        }
    }
}
