﻿using Imdb.Core.Authentication;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Imdb.Application.Authentication
{
    public class AuthenticatedUser : IAuthenticatedUser
    {
        private readonly IHttpContextAccessor _acessor;

        public AuthenticatedUser(IHttpContextAccessor accessor)
        {
            _acessor = accessor;
        }

        public Guid Id
        {
            get
            {
                if (GetClaims().FirstOrDefault(e => e.Type == ClaimTypes.NameIdentifier)?.Value is not string id || string.IsNullOrEmpty(id))
                    return Guid.Empty;

                return Guid.Parse(id);
            }
        }

        public string[] Roles => GetClaims().Where(e => e.Type == ClaimTypes.Role).Select(e => e.Value).ToArray();
        public string Name => GetClaims().FirstOrDefault(e => e.Type == ClaimTypes.Name)?.Value;
        public string Email => GetClaims().FirstOrDefault(e => e.Type == ClaimTypes.Email)?.Value;

        private IEnumerable<Claim> GetClaims() => _acessor?.HttpContext?.User?.Claims ?? Array.Empty<Claim>();
    }
}
