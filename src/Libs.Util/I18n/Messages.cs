﻿namespace Libs.Util.I18n
{

    public class Messages
    {
        public class User
        {
            public const string UserAlreadyExists = "@Messages.User.UserAlreadyExists";
            public const string UserCreatedWithSuccess = "@Messages.User.UserCreatedWithSuccess";
            public const string PasswordChangedWithSuccess = "@Messages.User.PasswordChanged";
        }
    }
}
