﻿using System.Security.Cryptography;
using System.Text;

namespace Libs.Util.Commum
{
    public class Crypt
    {
        public static string ToSha256(string value)
        {
            var hash = SHA256.Create();
            var utf8Value = Encoding.UTF8.GetBytes(value);
            var encryptedValue = hash.ComputeHash(utf8Value);
            var stringBuilder = new StringBuilder();

            foreach (var character in encryptedValue)
                stringBuilder.Append(character.ToString("X2"));

            return stringBuilder.ToString();
        }
    }
}
