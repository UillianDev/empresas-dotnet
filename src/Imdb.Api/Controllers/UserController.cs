﻿using Imdb.Api.Contracts;
using Imdb.Application.Commands.Requests.User;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Api.Controllers
{
    [Route("api/v1/[controller]")]
    public class UserController : AbstractController
    {
        public UserController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        [HttpPost]
        public Task<IActionResult> Register([FromBody] RegisterUserCommand command, CancellationToken cancellationToken) => SendAsync(command, cancellationToken);

        [HttpPut]
        public Task<IActionResult> Update([FromBody] UpdateUserCommand command, CancellationToken cancellationToken) => SendAsync(command, cancellationToken);

        [HttpDelete("{userId}")]
        public Task<IActionResult> Delete([FromRoute] Guid userId, CancellationToken cancellationToken) => SendAsync(new DeleteUserCommand(userId), cancellationToken);

    }
}
