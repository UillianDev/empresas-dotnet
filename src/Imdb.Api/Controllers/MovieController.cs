﻿using Imdb.Api.Contracts;
using Imdb.Application.Commands.Requests.Movie;
using Imdb.Application.Queries.Requests.Movies;
using Imdb.Core.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [Produces("application/json")]
    public class MovieController : AbstractController
    {
        public MovieController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        [HttpPost]
        [Authorize(Roles = ImdbRoles.Administrator)]
        public Task<IActionResult> RegisterAsync([FromBody] RegisterMovieCommand command, CancellationToken cancellationToken) => SendAsync(command, cancellationToken);

        [HttpGet]
        public Task<IActionResult> Get([FromQuery] GetMovies command, CancellationToken cancellationToken) => QueryListAsync(command, cancellationToken);

        [HttpGet("{movieId}/[action]")]
        public Task<IActionResult> Details([FromRoute] Guid movieId, CancellationToken cancellationToken) => QueryFirstAsync(new GetMovieDatails(movieId), cancellationToken);

        [HttpPut("{movieId}/[action]")]
        [Authorize(Roles = ImdbRoles.User)]
        public Task<IActionResult> Vote([FromRoute] Guid movieId, [FromBody] VoteCommand command, CancellationToken cancellationToken)
        {
            command.MovieId = movieId;
            return SendAsync(command, cancellationToken);
        }
    }
}
