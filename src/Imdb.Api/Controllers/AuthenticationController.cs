﻿using Imdb.Api.Contracts;
using Imdb.Application.Commands.Requests.Authentication;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Api.Controllers
{
    [Route("api/v1/[controller]")]
    public class AuthenticationController : AbstractController
    {
        public AuthenticationController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        [HttpPost]
        public Task<IActionResult> AuthenticateAsync([FromBody] AuthenticateCommand command, CancellationToken cancellationToken) => SendAsync(command, cancellationToken);
    }
}
