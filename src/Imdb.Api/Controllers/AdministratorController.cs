﻿using Imdb.Api.Contracts;
using Imdb.Application.Commands.Requests.Administrator;
using Imdb.Application.Commands.Requests.User;
using Imdb.Application.Queries.Requests.Administrator;
using Imdb.Core.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [Authorize(Roles = ImdbRoles.Administrator)]
    public class AdministratorController : AbstractController
    {
        public AdministratorController(IServiceProvider serviceProvider) : base(serviceProvider)
        {
        }

        [HttpPost]
        public Task<IActionResult> RegisterAsync([FromBody] RegisterAdministratorCommand command, CancellationToken cancellationToken) =>
            SendAsync(command, cancellationToken);

        [HttpPut]
        public Task<IActionResult> UpdateAsync([FromBody] UpdateUserCommand command, CancellationToken cancellationToken) =>
            SendAsync(command, cancellationToken);

        [HttpDelete("{administratorId}")]
        public Task DeleteAsync([FromRoute] Guid administratorId, CancellationToken cancellationToken) => SendAsync(new DeleteUserCommand(administratorId), cancellationToken);

        [HttpGet("users")]
        public Task<IActionResult> UsersAsync([FromQuery] GetActiveNormalUsers query, CancellationToken cancellationToken) => QueryListAsync(query, cancellationToken);
    }
}
