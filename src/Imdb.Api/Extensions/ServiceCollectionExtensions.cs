﻿using Imdb.Application.Authentication;
using Imdb.Application.Handlers;
using Imdb.Application.Interceptors;
using Imdb.Core.Authentication;
using Imdb.Core.Facades;
using Imdb.Core.Repositories;
using Imdb.Infra.Facades;
using Imdb.Infra.Handlers;
using Imdb.Infra.Repositories;
using Libs.Cqrs.Contracts;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static void AddImdbDI(this IServiceCollection services)
        {
            services.AddMediator(m => m.FromAssemblyOf<UserHandler>()
                .AddOutputInterceptor<UnitOfWorkInterceptor>()
                .AddInterceptor<ValidatorInterceptor>());

            services.AddHttpContextAccessor();
            services.AddScoped<IQueryHandler, QueryHandler>();
            services.AddScoped<IRepository, Repository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IAuthenticatedUser, AuthenticatedUser>();
        }
    }


}
