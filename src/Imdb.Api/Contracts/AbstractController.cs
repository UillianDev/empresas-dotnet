﻿using Imdb.Core.Cqrs;
using Libs.Cqrs.Contracts;
using Libs.Mediator.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Imdb.Api.Contracts
{
    public class AbstractController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IQueryHandler _queryHandler;

        protected AbstractController(IServiceProvider serviceProvider)
        {
            _mediator = serviceProvider.GetRequiredService<IMediator>();
            _queryHandler = serviceProvider.GetRequiredService<IQueryHandler>();
        }

        public Task<IActionResult> QueryListAsync<T, R>(IQuery<T, R> query, CancellationToken cancellationToken) where T : class where R : class =>
            ExecuteSafeAsync(() => _queryHandler.Handle(query).ToListAsync(cancellationToken));

        public Task<IActionResult> QueryFirstAsync<T, R>(IQuery<T, R> query, CancellationToken cancellationToken) where T : class where R : class =>
            ExecuteSafeAsync(() => _queryHandler.Handle(query).FirstOrDefaultAsync(cancellationToken));

        public IQueryable<R> QueryRaw<T, R>(IQuery<T, R> query) where T : class where R : class => _queryHandler.Handle(query);

        public Task<IActionResult> QueryListAsync<T>(IQuery<T> query, CancellationToken cancellationToken) where T : class =>
            ExecuteSafeAsync(() => _queryHandler.Handle(query).ToListAsync(cancellationToken));

        public Task<IActionResult> QueryFirstAsync<T>(IQuery<T> query, CancellationToken cancellationToken) where T : class =>
            ExecuteSafeAsync(() => _queryHandler.Handle(query).FirstOrDefaultAsync(cancellationToken));

        public IQueryable<T> QueryRaw<T>(IQuery<T> query) where T : class => _queryHandler.Handle(query);

        public Task<IActionResult> SendAsync<T>(T command, CancellationToken cancellationToken) where T : ICommandInput =>
            ExecuteSafeAsync(() => _mediator.SendAsync(command, cancellationToken));

        private async Task<IActionResult> ExecuteSafeAsync<T>(Func<Task<T>> function)
        {
            try
            {
                var output = await function();
                return Ok(output);
            }
            catch (Exception ex)
            {
                return BadRequest(CommandResponse.Failure(ParseException(ex), ex.Message));
            }
        }

        private object ParseException(Exception ex)
        {
            object innerException = null;

            if (ex.InnerException != null)
                innerException = ParseException(ex.InnerException);

            return new
            {
                ex.Message,
                ex.StackTrace,
                InnerException = innerException
            };

        }
    }
}
