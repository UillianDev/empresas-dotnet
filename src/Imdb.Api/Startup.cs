using FluentValidation.AspNetCore;
using Imdb.Application.Commands.Validators.Administrator;
using Imdb.Core.Authentication;
using Imdb.Infra.Contexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Imdb.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration) => _configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            var corsOrigns = _configuration.GetValue<string[]>("Cors:Origns");
            var connectionString = _configuration.GetConnectionString("Imdb");

            services.AddDbContext<ImdbDbContext>((serviceProvider, ctxBuilder) => ctxBuilder
                    .UseSqlServer(connectionString, npgsqlOpts => npgsqlOpts
                        .MigrationsAssembly("Imdb.Infra"))
                        .UseLazyLoadingProxies());


            services.AddImdbDI();
            services.AddRouting(options =>
            {
                options.LowercaseUrls = true;
                options.LowercaseQueryStrings = true;
            });

            services.AddMvc(opts => opts.EnableEndpointRouting = false)
                .AddFluentValidation(config => config.RegisterValidatorsFromAssemblyContaining<RegisterAdministratorCommandValidator>());

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = AuthenticationConfigurations.Issuer,
                    ValidAudience = AuthenticationConfigurations.Audience,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AuthenticationConfigurations.CryptKey)),
                    ValidateIssuer = true,
                    ValidateAudience = true
                };
            });

            

            services.AddCors(opts => opts
                .AddDefaultPolicy(policy => policy
                    .WithOrigins(corsOrigns)
                    .AllowAnyHeader()
                    .AllowAnyMethod()));

            services.AddSwaggerGen(opts =>
            {
                var name = _configuration["Swagger:Doc:Name"];
                var title = _configuration["Swagger:Doc:Title"];
                var version = _configuration["Swagger:Doc:Version"];

                opts.SwaggerDoc(name, new OpenApiInfo
                {
                    Title = title,
                    Version = version,
                });


                opts.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please add the authentication token",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                });

                opts.OperationFilter<AuthorizeCheckOperationFilter>();
            });

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();

            app.UseSwagger();
            app.UseSwaggerUI(setup =>
            {
                var endpoint = _configuration["Swagger:Doc:Endpoint"];
                var name = _configuration["Swagger:Doc:Name"];
                setup.SwaggerEndpoint(endpoint, name);
            });

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseStaticFiles();
            app.UseMvc();
            app.UseRouting();
            ApplyMigrations(app);
        }

        private static void ApplyMigrations(IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.CreateScope();
            var serviceProvider = scope.ServiceProvider;
            var dbContext = serviceProvider.GetRequiredService<ImdbDbContext>();
            dbContext.Database.Migrate();
        }
    }

    public class AuthorizeCheckOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            var hasAuthorize =
              context.MethodInfo.DeclaringType.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any()
              || context.MethodInfo.GetCustomAttributes(true).OfType<AuthorizeAttribute>().Any();

            if (hasAuthorize)
            {
                operation.Responses.Add("401", new OpenApiResponse { Description = "Unauthorized" });
                operation.Responses.Add("403", new OpenApiResponse { Description = "Forbidden" });
                operation.Security = new List<OpenApiSecurityRequirement>
                {
                    new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer"
                                }
                            },
                            Array.Empty<string>()
                        }
                    }
                };

            }
        }
    }
}
